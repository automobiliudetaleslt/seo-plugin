# OmniSeoPlugin

![Seo information](img/seo_information.png?raw=true "Seo Information")

A plugin that allows the formation of header information such as title, metatags and more 
to be formed dynamically. It comes with a handy set of twig functions that you can use in
your templates. 

## Installation

### Add the plugin to Kernel
 
 ```php
 # AppKernel.php
 
 public function registerBundles()
 {
   $bundles = [
     // ...
     new \Omni\Sylius\SeoPlugin\OmniSyliusSeoPlugin(),
     // ...
   ];
 }
 ```
 
And that's it! That's really all you need to do to get started :) 
  
## Getting started

At this point you have a fully functioning bundle, however, you still need to configure 
the resources you want to be seo aware in order to make any use of the bundle at all.
Let's start with something easy and configure the content nodes of `OmniSyliusCmsPlugin`
to have seo functionality.

### Configuring Node resources

This is easy because the Omni package is already integrated and fully configurable.
That means that all you need to do is to go to your `omni_cms` configuration and add
`seo_aware` flag on te node types that you want:

```yaml
omni_sylius_cms:
    node_types:
        content:
            content: true
            multiple: true
            seo_aware: true
```

And that's it, now all content nodes will be seo aware, you can test it 
by going to the edit page of such nodes in the admin. You should see a lot of
possible configuration for the seo metadata. Try adding some metatag information
and going to the page of this content node. If you will inspect its elements you should
see your new metatags in the head section of the page.

### Configuring other resources

It takes a bit more work in order to implement the seo functionality for other resources.
Let's imagine that you want to add seo functionality to products and divide the implementation
scenario into sections:

### 1. Extend the translation of the resource

You will need to use the sylius resource debug command to find out which class is used for
the translation. Run:

```bash
$ bin/console sylius:debug:resource sylius.product_translation
```

Find out which class is used for the translation and extend it in AppBundle. In this example
we will assume that the class being used is `Sylius\Component\Core\Model\ProductTranslation`.
Now override the class:

```php
<?php

namespace AppBundle\Entity;

use Omni\Sylius\SeoPlugin\Model\SeoAwareTranslationInterface;
use Omni\Sylius\SeoPlugin\Model\SeoAwareTranslationTrait;
use Sylius\Component\Core\Model\ProductTranslation as BaseTranslation;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_translation")
 */
class ProductTranslation extends BaseTranslation implements SeoAwareTranslationInterface
{
    use SeoAwareTranslationTrait;
}
```

Note that it is very important to implement `SeoAwareTranslationInterface`. Also, if you
don't need any custom implementation you should use `SeoAwareTranslationTrait`.

### 2. Extend the resource itself:

Similarly, do the same thing for the `Product` class itself, only use different interface
and trait:

```php
<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Product as BaseProduct;
use Omni\Sylius\SeoPlugin\Model\SeoAwareInterface;
use Omni\Sylius\SeoPlugin\Model\SeoAwareTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product")
 */
class Product extends BaseProduct implements SeoAwareInterface
{
    use SeoAwareTrait;
}
```
Here we assume that the base class of the product is `Sylius\Component\Core\Model\Product`
but you should check it with sylius debug command. Note that it should implement `SeoAwareInterface`
and use the `SeoAwareTrait` for convenience.

### 3. Define the overridden resources in configuration

Add these lines to your `config.yml` file:

```yaml
# app/config/cofig.yml

sylius_product:
    resources:
        product:
            classes:
                model: AppBundle\Entity\Product
            translation:
                classes:
                    model: AppBundle\Entity\ProductTranslation
```

### 4. Update the mapping

At this point you need to update your database mapping:
 
```bash
$ bin/console doctrine:schema:update --force
```

> Note that you don't need to provide any additional mapping information, it is all 
handled by the omni seo plugin

### 5. Extend the form:

Now that you have updated the models, you must add a form extension for the `ProductTranslation`
so that you can add the metadata information in the administration section of the application.

Let's begin by adding a form extension:

```php
<?php

namespace AppBundle\Form\Extension;

use Omni\Sylius\SeoPlugin\Form\Extension\AbstractSeoMetadataExtension;
use Sylius\Bundle\ProductBundle\Form\Type\ProductTranslationType;

class ProductTranslationTypeExtension extends AbstractSeoMetadataExtension
{
    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return ProductTranslationType::class;
    }
}

```

As you can see, all you need to do, unless you need some custom logic, is to extend
`AbstractSeoMetadataExtension` and define the class of the type you are extending from.

Next you need to register it as a service:

```yaml
# services.yml
services:
    app.form.extension.type.customer_profile:
        class: AppBundle\Form\Extension\ProductTranslationTypeExtension
        tags:
            - { name: form.type_extension, extended_type: Sylius\Bundle\ProductBundle\Form\Type\ProductTranslationType }
```

> please note that you should also check the exact type for extending with the sylius 
debug command.

### 6. Setting view data parameters

> Notice: please skip this step if the resource in question does not use a custom controller

This step is only needed when the resource that you want to implement seo functionality to
uses a custom controller that implements custom parameters to pass to the view object. The 
sylius resource controller passes the resource in question to the view by the parameter name
of `resource`. However, you could have a custom controller for, lets say, a resource named
`article`. The snippet from such controller action could look like this:

```php
$view
    ->setTemplate('...')
    ->setData([
        // ...
        'article'         => $article,
        // ...
    ]);
```

As you can see, the object in question is being passed with the parameter name `article`
and you need to tell the `OmniSyliusSeoPlugin` that this is so by setting a parameter 
in your `config.yml`:

```yaml
omni_sylius_seo:
    seo_view_data_parameters: [resource, article]
```

Please note that the default value of this parameter is `[resource]`, therefore you 
should include it when you override the parameter.

### 7. Templating

Lastly, you need to extend sylius shop product or layout templates and add omni seo
twig rendering functions to the head section of the templates. This step is entirely up 
to you, however, as example you could simply override the sylius shop bundle layout template
and change it with an example one from the plugin like so:

```twig
{#app/Resources/SyliusShopBundle/views/layout.html.twig#}

{% include 'OmniSyliusSeoPlugin::layout.html.twig' %}
```

This will load your page titles and all the seo meta tags from your configuration
in the back end of the application.

## The list of available twig functions

* `omni_seo_head_attributes`
* `omni_seo_html_attributes`
* `omni_seo_lang_alternates`
* `omni_seo_link_canonical`
* `omni_seo_links`
* `omni_seo_meta`
* `omni_seo_title` (you can pass optional string parameter for default value)
