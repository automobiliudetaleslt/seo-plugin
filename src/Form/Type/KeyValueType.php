<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Form\Type;

use Omni\Sylius\SeoPlugin\Form\DataTransformer\KeyValueTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KeyValueType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $preSetDataListener = function (FormEvent $event) {
            if (null === $event->getData()) {
                return null;
            }

            $data = [];
            foreach ($event->getData() as $key => $value) {
                $data[] = ['key' => $key, 'value' => $value];
            }
            $event->setData($data);
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, $preSetDataListener, 1);
        $builder->addModelTransformer(new KeyValueTransformer($options['use_key_value_array']));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'type' => KeyValueRowType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'allowed_keys' => null,
                'label' => false,
                'value_options' => [],
                'use_key_value_array' => false,
                'options' => function (Options $options) {
                    return [
                        'value_type' => $options['value_type'],
                        'value_options' => $options['value_options'],
                        'allowed_keys' => $options['allowed_keys'],
                        'label' => false,
                    ];
                }
            ]
        );

        $resolver->setAllowedTypes('allowed_keys', ['null', 'array']);
        $resolver->setRequired(['value_type']);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return CollectionType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'omni_seo_key_value';
    }
}
