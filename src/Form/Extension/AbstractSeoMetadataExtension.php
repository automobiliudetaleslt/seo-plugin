<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Form\Extension;

use Omni\Sylius\SeoPlugin\Form\Type\SeoMetadataType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

abstract class AbstractSeoMetadataExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('seoMetadata', SeoMetadataType::class, [
            'label' => 'omni_sylius.form.node.metadata',
            'required' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    abstract public function getExtendedType();
}
